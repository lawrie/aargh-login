module.exports = {
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^(?!app)/,
        'app.js': /^app/
      }
    },
    stylesheets: {joinTo: 'app.css'}
  },
  modules: {
    definition: false,
    wrapper: false
    //autoRequire: {
    //  'app.js': ['app']
    //}
  },

  paths: {
    public: 'www'
  },

  plugins: {
    babel: {presets: ['es2015', 'stage-0']}
  }
};
