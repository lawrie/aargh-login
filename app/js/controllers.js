(function () {
    'use strict';

    angular
        .module('starter')
        .controller('DashCtrl', DashCtrl)
        .controller('ChatsCtrl', ChatsCtrl)
        .controller('ChatDetailCtrl', ChatDetailCtrl)
        .controller('AccountCtrl', AccountCtrl);

    DashCtrl.$inject = ['$scope'];

    ChatsCtrl.$inject = ['$scope', 'Chats'];

    ChatDetailCtrl.$inject = ['$scope', '$stateParams', 'Chats'];

    AccountCtrl.$inject = ['$scope'];

    function DashCtrl ($scope) {

    }

    function ChatsCtrl ($scope, Chats) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //
        //$scope.$on('$ionicView.enter', function(e) {
        //});
        $scope.chats = Chats.all();
        $scope.remove = function(chat) {
            Chats.remove(chat);
        };
    }

    function ChatDetailCtrl ($scope, $stateParams, Chats) {
        $scope.chat = Chats.get($stateParams.chatId);
    }

    function AccountCtrl ($scope) {
        $scope.settings = {
            enableFriends: true
        };
    }
})();
